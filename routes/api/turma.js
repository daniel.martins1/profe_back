const express = require('express');
const Turma = require('../../models/turma');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const MSGS = require('../../messages')
//const files = require('../../middleware/file')


// @route    GET /category/:id
// @desc     DETAIL category
// @access   Public
router.get('/:id',[], async (req, res, next) => {
    try {
        const id = req.params.id
        const turma = await Turma.find({profe: id})
        .populate({
          path: 'profe',
          populate: {path:'profe'}
        })
      .populate({
          path: 'disciplina',
          populate: { path: 'disciplina'}
      });
        if(turma){
            res.json(turma)
        }else{
            res.status(404).send({ "error": MSGS.CATEGORY404 }) 
        }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})


// @route    PATCH /category/:id
// @desc     PARTIAL UPDATE category
// @access   Public
router.patch('/:id',[], async (req, res, next) => {
  try {
      const turma = await Turma.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
      if(turma){
          res.json(turma
            )
      }else{
          res.status(404).send({ "error": MSGS.CATEGORY404 }) 
      }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

// @route    DELETE /category/:id
// @desc     DELETE category
// @access   Public
router.delete('/:id',[], async (req, res, next) => {
  try {
      const id = req.params.id
      const turma = await Turma.findOneAndDelete({_id : id})
      if(turma){
          res.json(turma)
      }else{
          res.status(404).send({ "error": MSGS.CATEGORY404 }) 
      }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


// @route    GET /category
// @desc     LIST category
// @access   Public
router.get('/', async (req, res, next) => {
    try {
      const turma = await Turma.find({})
      .populate({
        path: 'profe',
        populate: {path:'profe'}
      })
    .populate({
        path: 'disciplina',
        populate: { path: 'disciplina'}
    });
      res.json(turma)
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
  })
  

// @route    POST /category
// @desc     CREATE category
// @access   Public
router.post('/', [
  check('disciplina').not().isEmpty(),
  check('profe').not().isEmpty(),
  check('comeca_date').not().isEmpty(),
  check('termina_date').not().isEmpty(),
  check('turno').not().isEmpty(),
  check('dias').not().isEmpty()
//    check('icon').not().isEmpty()
  ], async (req, res, next) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
      } else {
        let turma = new Turma(req.body)
        await turma.save()
        if (turma.id) {
          res.json(turma);
        }
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
  })


module.exports = router;