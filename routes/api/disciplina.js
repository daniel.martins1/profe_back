const express = require('express');
const Disciplina = require('../../models/disciplina');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const MSGS = require('../../messages')
//const files = require('../../middleware/file')


// @route    GET /category/:id
// @desc     DETAIL category
// @access   Public
router.get('/:id',[], async (req, res, next) => {
    try {
        const id = req.params.id
        const disciplina = await Disciplina.findOne({_id : id})
        if(disciplina){
            res.json(disciplina)
        }else{
            res.status(404).send({ "error": MSGS.CATEGORY404 }) 
        }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})


// @route    PATCH /category/:id
// @desc     PARTIAL UPDATE category
// @access   Public
router.patch('/:id',[], async (req, res, next) => {
  try {
      const disciplina = await Disciplina.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
      if(disciplina){
          res.json(disciplina)
      }else{
          res.status(404).send({ "error": MSGS.CATEGORY404 }) 
      }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

// @route    DELETE /category/:id
// @desc     DELETE category
// @access   Public
router.delete('/:id',[], async (req, res, next) => {
  try {
      const id = req.params.id
      const disciplina = await Disciplina.findOneAndDelete({_id : id})
      if(disciplina){
          res.json(disciplina)
      }else{
          res.status(404).send({ "error": MSGS.CATEGORY404 }) 
      }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


// @route    GET /category
// @desc     LIST category
// @access   Public
router.get('/', async (req, res, next) => {
    try {
      const disciplina = await Disciplina.find({})
      res.json(disciplina)
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
  })
  

// @route    POST /category
// @desc     CREATE category
// @access   Public
router.post('/', [
    check('name').not().isEmpty()
//    check('icon').not().isEmpty()
  ], async (req, res, next) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
      } else {
        let { name, curso } = req.body
        console.log(req.body)
        let disciplina = new Disciplina({ name, curso })
        await disciplina.save()
        if (disciplina.id) {
          res.json(disciplina);
        }
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
  })


module.exports = router;