const express = require('express');
const Profe = require('../../models/profe');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const MSGS = require('../../messages')
const bcrypt = require('bcryptjs');
//const files = require('../../middleware/file')


// @route    GET /category/:id
// @desc     DETAIL category
// @access   Public
router.get('/:id',[], async (req, res, next) => {
    try {
        const id = req.params.id
        const profe = await Profe.findOne({_id : id})
        if(profe){
            res.json(profe)
        }else{
            res.status(404).send({ "error": MSGS.CATEGORY404 }) 
        }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})


// @route    PATCH /category/:id
// @desc     PARTIAL UPDATE category
// @access   Public
router.patch('/:id',[], async (req, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      res.status(400).send({ errors: errors.array() })
      return
    }
   // const id = request.params.userId
    const salt = await bcrypt.genSalt(10)
    
    let bodyRequest = req.body

    if(bodyRequest.password){
      bodyRequest.password = await bcrypt.hash(bodyRequest.password, salt)
    }
  //  const update = { $set: bodyRequest }
      const profe = await Profe.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
      if(profe){
          res.json(profe)
      }else{
          res.status(404).send({ "error": MSGS.CATEGORY404 }) 
      }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

// @route    DELETE /category/:id
// @desc     DELETE category
// @access   Public
router.delete('/:id',[], async (req, res, next) => {
  try {
      const id = req.params.id
      const profe = await Profe.findOneAndDelete({_id : id})
      if(profe){
          res.json(profe)
      }else{
          res.status(404).send({ "error": MSGS.CATEGORY404 }) 
      }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


// @route    GET /category
// @desc     LIST category
// @access   Public
router.get('/', async (req, res, next) => {
    try {
      const profe = await Profe.find({})
      res.json(profe)
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
  })
  

// @route    POST /category
// @desc     CREATE category
// @access   Public
router.post('/', [
    check('name').not().isEmpty(),
    check('email', MSGS.VALID_EMAIL).isEmail(),
   check('password', MSGS.PASSWORD_VALIDATED).isLength({ min: 6 })
//    check('icon').not().isEmpty()
  ], async (req, res, next) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
      } else {
        let { name, email, password, isadmin } = req.body
        console.log(req.body)
        let profe = new Profe({ name, email, password, isadmin })
        const salt = await bcrypt.genSalt(10);
        profe.password = await bcrypt.hash(password, salt);
        await profe.save()
        if (profe.id) {
          res.json(profe);
        }
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": err })
    }
  })


module.exports = router;