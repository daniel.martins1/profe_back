const mongoose = require('mongoose');

const DisciplinaSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    curso: {
        type: String,
//        required: true
    }
}, { autoCreate : true })

module.exports = mongoose.model('disciplina', DisciplinaSchema);
