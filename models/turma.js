const mongoose = require('mongoose');

const TurmaSchema = new mongoose.Schema({

    disciplina:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'disciplina',
        required : true
    },
    profe:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'profe',
        required : true
    },
    comeca_date: {
        type: Date,
        required : true
    },
    termina_date: {
        type: Date,
        required : true
    },
    turno: {
        type : String,
        required : true
    },
    dias: {
        type : String,
        required : true
    }
}, { autoCreate : true })

module.exports = mongoose.model('turma', TurmaSchema);
