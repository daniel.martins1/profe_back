const mongoose = require('mongoose');

const ProfeSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    email: {
        type: String,
       required: true
    },
    password :{
        type: String,
        required: true,
    },
    isadmin : {
        type: Boolean,
        default: false
    }
}, { autoCreate : true })

module.exports = mongoose.model('profe', ProfeSchema);
